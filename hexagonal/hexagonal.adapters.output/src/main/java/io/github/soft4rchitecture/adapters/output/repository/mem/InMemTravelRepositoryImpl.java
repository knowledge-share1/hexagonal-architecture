package io.github.soft4rchitecture.adapters.output.repository.mem;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Priority;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Alternative;

import io.github.soft4rchitecture.domain.core.entity.BookingInformation;
import io.github.soft4rchitecture.domain.core.entity.Travel;
import io.github.soft4rchitecture.domain.ports.output.repository.TravelRepository;

@Alternative
@Priority(2)
@ApplicationScoped
public class InMemTravelRepositoryImpl implements TravelRepository {

	private final Map<String, Travel> store;

	public InMemTravelRepositoryImpl() {
		this.store = new HashMap<>();
	}

	@Override
	public Travel findById(String id) {
		Travel travel = store.get(id);
		if (travel == null) {
			throw new RuntimeException("Travel with id" + id + " was not found");
		}

		return travel;
	}

	@Override
	public Travel create(String uuid, BookingInformation information) {

		this.store.put(uuid, new Travel(uuid));
		return this.store.get(uuid);
	}

}
