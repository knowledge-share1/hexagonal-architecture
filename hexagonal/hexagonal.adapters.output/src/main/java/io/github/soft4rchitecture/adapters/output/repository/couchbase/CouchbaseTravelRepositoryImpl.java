package io.github.soft4rchitecture.adapters.output.repository.couchbase;

import javax.annotation.Priority;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Alternative;

import io.github.soft4rchitecture.domain.core.entity.BookingInformation;
import io.github.soft4rchitecture.domain.core.entity.Travel;
import io.github.soft4rchitecture.domain.ports.output.repository.TravelRepository;

@Alternative
@Priority(1)
@ApplicationScoped
public class CouchbaseTravelRepositoryImpl implements TravelRepository {

	@Override
	public Travel findById(String id) {
		return null;
	}

	@Override
	public Travel create(String uuid, BookingInformation information) {
		// TODO Auto-generated method stub
		return null;
	}

}
