package io.github.soft4rchitecture.adapters.input.rest;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import io.github.soft4rchitecture.domain.core.entity.BookingInformation;
import io.github.soft4rchitecture.domain.core.entity.Travel;
import io.github.soft4rchitecture.domain.ports.input.CreateTravelUseCase;
import io.github.soft4rchitecture.domain.ports.input.FindTravelUseCase;

@Path("travel")
@Produces(MediaType.APPLICATION_JSON)
public class TravelRestController {

	private CreateTravelUseCase usecase;
	private FindTravelUseCase findTravelUseCase;

	@Inject
	public TravelRestController(CreateTravelUseCase usecase, FindTravelUseCase findTravelUseCase) {
		this.usecase = usecase;
		this.findTravelUseCase = findTravelUseCase;
	}

	@GET
	@Path(("/{id}"))
	public Travel getTravel(@PathParam("id") String id) {
		return this.findTravelUseCase.execute(id);
	}

	@POST
	public Response postTravel(@Context UriInfo uri, BookingInformationDTO travelDto) {

		Travel newTravel = usecase.execute(new BookingInformation());
		return Response
				.created(UriBuilder.fromUri(uri.getRequestUri()).path(newTravel.getTravelId()).build())
				.build();

	}

}
