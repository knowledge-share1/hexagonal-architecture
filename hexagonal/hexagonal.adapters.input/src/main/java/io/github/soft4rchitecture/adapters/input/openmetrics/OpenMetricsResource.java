package io.github.soft4rchitecture.adapters.input.openmetrics;

import java.util.LinkedList;

import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import io.micrometer.core.instrument.MeterRegistry;

@Path("/example")
@Produces("text/plain")
public class OpenMetricsResource {

	private final MeterRegistry registry;
	private LinkedList<Long> list = new LinkedList<>();

	OpenMetricsResource(MeterRegistry registry) {
		this.registry = registry;
		// registry.gaugeCollectionSize("example.list.size", Tags.empty(), list);
	}

	// @GET
	// @Path("gauge/{number}")
	// public Long checkListSize(long number) {
	// if (number == 2 || number % 2 == 0) {
	// // add even numbers to the list
	// list.add(number);
	// } else {
	// // remove items from the list for odd numbers
	// try {
	// number = list.removeFirst();
	// } catch (NoSuchElementException nse) {
	// number = 0;
	// }
	// }
	// return number;
	// }

}
