package io.github.soft4rchitecture.domain.ports.input;

@FunctionalInterface
public interface UseCase<P, R> {

	R execute(P parameter);
	
	
}
