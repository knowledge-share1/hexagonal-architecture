package io.github.soft4rchitecture.domain.ports.input;

import io.github.soft4rchitecture.domain.core.entity.BookingInformation;
import io.github.soft4rchitecture.domain.core.entity.Travel;

public interface CreateTravelUseCase extends UseCase<BookingInformation, Travel> {
}
