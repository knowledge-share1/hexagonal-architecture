package io.github.soft4rchitecture.domain.ports.input;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import io.github.soft4rchitecture.domain.core.entity.Travel;
import io.github.soft4rchitecture.domain.ports.output.repository.TravelRepository;

@ApplicationScoped
public class FindTravelUseCaseImpl implements FindTravelUseCase {

	
	
	@Inject
	TravelRepository travelRepository;
	
	
	@Override
	public Travel execute(String id) {
		return this.travelRepository.findById(id);
	}

}
