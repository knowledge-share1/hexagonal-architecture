package io.github.soft4rchitecture.domain.ports.input;

import java.util.UUID;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import io.github.soft4rchitecture.domain.core.entity.BookingInformation;
import io.github.soft4rchitecture.domain.core.entity.Travel;
import io.github.soft4rchitecture.domain.ports.output.repository.TravelRepository;

@ApplicationScoped
public class CreateTravelUseCaseImpl implements CreateTravelUseCase {

	@Inject
	TravelRepository travelRepository;
	
	@Override
	public Travel execute(BookingInformation information) {
		UUID randomUUID = UUID.randomUUID();
		return this.travelRepository.create(randomUUID.toString(), information);
	}

}
