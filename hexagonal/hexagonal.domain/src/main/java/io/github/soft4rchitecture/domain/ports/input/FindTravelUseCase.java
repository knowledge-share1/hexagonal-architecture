package io.github.soft4rchitecture.domain.ports.input;

import io.github.soft4rchitecture.domain.core.entity.Travel;

public interface FindTravelUseCase extends UseCase<String, Travel> {
}
