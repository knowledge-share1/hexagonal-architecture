package io.github.soft4rchitecture.domain.ports.output.repository;

import io.github.soft4rchitecture.domain.core.entity.BookingInformation;
import io.github.soft4rchitecture.domain.core.entity.Travel;

public interface TravelRepository {

	Travel create(String uuid, BookingInformation information);

	Travel findById(String id);

}
