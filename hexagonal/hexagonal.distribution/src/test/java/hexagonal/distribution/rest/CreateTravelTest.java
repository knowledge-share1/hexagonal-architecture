package hexagonal.distribution.rest;

import static io.restassured.RestAssured.when;
import static org.assertj.core.api.Assertions.assertThat;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import javax.inject.Inject;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.rest.client.RestClientBuilder;
import org.eclipse.microprofile.rest.client.RestClientDefinitionException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import io.github.soft4rchitecture.domain.core.entity.BookingInformation;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.RestAssured;

@QuarkusTest
class CreateTravelTest {

	@Inject
	@ConfigProperty(name = "quarkus.http.test-port")
	String serverPort;

	@Inject
	private ObjectMapper mapper;

	private String travelId;

	@BeforeEach
	public void setUp()
			throws IllegalStateException, RestClientDefinitionException, MalformedURLException, URISyntaxException {
		mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		HexagonalRestClient client = RestClientBuilder.newBuilder().baseUrl(new URL("http://localhost:" + serverPort))
				.build(HexagonalRestClient.class);
		final String headerString = client.createNewTravel(new BookingInformation()).getHeaderString("Location");
		this.travelId = getLastSegmentOfUri(headerString);

	}

	@Test
	void givenNonExistingTravel_thenReturn2404StatusCode() throws URISyntaxException {
		RestAssured.when().get("/travel/thisTravelCodeDoesNotExist").then().statusCode(404);
	}

	@Test
	void givenExistingTravel_thenReturn200StatusCode() throws URISyntaxException {

		when().get("/travel/" + travelId).then().statusCode(200);
	}

	@Test
	void givenExistingTravel_thenReturnTravelWithIdItsOwnTravelId() throws URISyntaxException {
		String id = when().get("/travel/" + travelId).getBody().jsonPath().getString("travelId");
		assertThat(id).isEqualTo(travelId);
	}

	private String getLastSegmentOfUri(String headerString) throws URISyntaxException {

		String[] segments = new URI(headerString).getPath().split("/");
		String idStr = segments[segments.length - 1];
		return idStr;
	}

}
