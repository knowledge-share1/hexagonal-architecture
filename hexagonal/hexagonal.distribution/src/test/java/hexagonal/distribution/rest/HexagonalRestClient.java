package hexagonal.distribution.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import io.github.soft4rchitecture.domain.core.entity.BookingInformation;
import io.github.soft4rchitecture.domain.core.entity.Travel;


@Path("/travel")
@Consumes("application/json")
public interface HexagonalRestClient {

    @GET
    @Path("/{id}")
    Travel getTravelById(@PathParam("id") String id);

    @POST
    Response createNewTravel(BookingInformation bookingInfo);
}